<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> <!-- Added Bootstrap to make reading the page more pleasant-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>
    <?php
    include('controller.php');

    $author = ''; //definition of static variables
    $title = '';
    $publisher = '';
    $year = '';
    ?>
    <div class="container">
        <!-- <div class='form-group'>
            <form method="post" action="books.php">
                <button type="submit" name="Disconnect" class="btn btn-outline-info">Disconnect</button>
            </form>
        </div> -->
        <?php
        echo displayInputInsert();
        ?>
        <div class='col-sm'>
            <?php echo listPublisher(); ?>
            <!-- Call listPublisher function to get the publisher list-->
        </div>
        <?php echo displayButtonInsert();
        echo displayInputSearch();
        ?>
        <div class='col-sm'>
            <?php echo listPublisher(); ?>
        </div>
        <?php
        echo displayButtonSearch();
        ?>
    </div>

    <div class="container">
        <?php


        function displayInputInsert()
        {
            echo "<div class='form-group'>
                <form method='post' class='form-inline' action='books.php'>
                    <!-- Send a form to books.php-->
                    <div class='col-sm'>
                        <label for='Title'></label>
                        <input type='text' class='form-control' placeholder='title' name='title'> <!-- creation of inputs with a bootstrap class-->
                    </div>
                    <div class='col-sm'>
                        <label for='Author'></label>
                        <input type='text' class='form-control' placeholder='author' name='author'>
                    </div>
                    <div class='col-sm'>
                        <label for='Year'></label>
                        <input type='year' class='form-control' placeholder='year' name='year'>
                    </div>";
        }

        function displayButtonInsert()
        {
            echo "<div class='col-sm'>
            <button type='submit' name='Insert' values='0' class='btn btn-outline-success'><i class='fas fa-pen'></i></button> <!-- Insert button creation -->
            </div>
            </form>
            </div>";
        }

        function displayInputSearch()
        {
            echo "<div class='form-group'>
            <form method='post' class='form-inline' action='books.php'> <!-- Same comments that the previous 'form-group'-->
                <div class='col-sm'>
                    <label for='Title'></label>
                    <input type='text' class='form-control' placeholder='title' name='title'>
                </div>
                <div class='col-sm'>
                    <label for='Author'></label>
                    <input type='text' class='form-control' placeholder='author' name='author'>
                </div>
                <div class='col-sm'>
                    <label for='Year'></label>
                    <input type='year' class='form-control' placeholder='year' name='year'>
                </div>";
        }

        function displayButtonSearch()
        {
            echo "<div class='col-sm'>
                <button type='submit' name='Search' values='0' class='btn btn-outline-success'><i class='fas fa-search'></i></button> <!-- Search button creation -->
                </div>
                </form>";
        }

        function displayTable($author, $title, $publisher, $year) //Function to display the book table of the DataBase
        {

            $elements = searchBooks($author, $title, $publisher, $year);
            $tableau = '';
            echo "<table class='table table-bordered'>
    <thead>
        <tr><th>Author</th><th>Title</th><th>Publisher</th><th>Year</th></tr>
    </thead>
    <tbody>";
            foreach ($elements as $element) { //recovery of the differents elements of the DataBase
                $tableau .= '
        <tr>
        <form method="post" action="books.php?ID=' . $element['ID'] . '">
        <td>' . $element['author'] . '</td>
        <td>' . $element['title'] . '</td>
        <td>' . $element['publisher'] . '</td>
        <td>' . $element['year'] . '</td>
        <td><button type="submit" name="Delete" class="btn btn-outline-danger"><i class="fas fa-trash"></i></button></td>
        </form>
        </tr>';
            }
            echo $tableau;
            echo "</tbody></table>";
        }
        //calls functions according to the form sent otherwise displays the whole books table
        if (isset($_POST['Insert'])) { //Form verification for the insert form

            $title = $_POST["title"];
            $author = $_POST["author"];
            $publisher = $_POST["publisher"];
            $year = $_POST["year"];
            createBook($title, $author, $publisher, $year);
        } elseif (isset($_POST['Delete'])) { //Form verification for the delete form

            if (isset($_GET['ID']) && !empty($_GET['ID']) && is_numeric($_GET['ID'])) {

                $IDbook = $_GET['ID'];
                deleteBook($IDbook);
            }
        } elseif (isset($_POST['DeleteAll'])) { //Form verification for the DeleteAll form
            $elements = searchBooks($author, $title, $publisher, $year);
            foreach ($elements as $element) {
                $author = $element['author'];
                DeleteAllBooks($author);
            }
        } elseif (isset($_POST['Search'])) { //Form verification for the Search form

            $title = $_POST["title"];
            $author = $_POST["author"];
            $publisher = $_POST["publisher"];
            $year = $_POST["year"];
            displayTable($author, $title, $publisher, $year);
        } else { //Else display the whole books table
            displayTable($author, $title, $publisher, $year);
        }

        function listPublisher() //Function to create a publisherList instead of an input
        {
            $pdo = connectDB();
            $sqlrequest = "SELECT publisher FROM publisher";
            $rst = $pdo->query($sqlrequest);
            $elements = $rst->fetchAll();
            echo "<select name='publisher' class='nav-link dropdown-toggle'><option></option>";

            foreach ($elements as $element) {
                $list .= '
        <option>' . $element['publisher'] . '</option>';
            }
            echo $list;
            echo "</select>";
        }

        ?>
    </div>
    <div class="container">
    <form method="post" action="books.php">
            <button type="submit" name="DeleteAll" class="btn btn-outline-danger">Delete All</button>
        </form>
    </div>
</body>

</html>