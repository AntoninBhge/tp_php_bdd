<?php
include("config.php");

function createBook($title, $author, $publisher, $year){ //Function to create a new book in the books table
    $pdo = connectDB();
    $add = $pdo->prepare("INSERT INTO books(author, title, publisher, year) VALUES ('$author', '$title', '$publisher','$year')");
    $add->execute();
    header("Location: books.php");
}

function deleteBook($IDbook){ //Function to delete a book in the books table
    $pdo = connectDB();
    $sqlrequest = $pdo->prepare("DELETE FROM books WHERE ID=:id ");
    $sqlrequest->bindParam(':id', intval($IDbook));
    $sqlrequest->execute();
    header("Location: books.php");
}

function searchBooks($author, $title, $publisher, $year){ //Function to search some books in the books table
    $pdo = connectDB();
    $search = $pdo->prepare("SELECT * FROM books WHERE author LIKE '%$author%' AND title LIKE '%$title%' AND publisher LIKE '%$publisher%' AND year LIKE '%$year%' ORDER BY author");
    $search->execute();
    $elements = $search->fetchAll();
    return $elements;
    header("Location: books.php");
}

function DeleteAllBooks($author){ //Function to delete all books in the books table
    $pdo = connectDB();

    $sqlrequest = $pdo->prepare("DELETE FROM books WHERE author = '$author' ");
    $sqlrequest->execute();
}



