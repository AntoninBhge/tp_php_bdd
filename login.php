<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <title>Document</title>
</head>

<body>
    <div id="container">

        <form action="connection.php" method="POST"> <!-- send a form to "connection.php" -->
            <h1>Connexion</h1>
            <?php
            if (isset($_GET['erreur']) && $_GET['erreur'] == 1) { //If the username or password is incorrect send a message
                
                echo "Utilisateur ou mot de passe incorrect";
            }

            if (isset($_GET['erreur']) && $_GET['erreur'] == 2) { //If the username or password is missing  send a message
                echo "utilisateur ou mot de passe vide";
            }
            ?>
            <div class="form-group"> <!--Input to enter the username -->
                <label for="usr">Username :</label>
                <input type="text" class="form-control" name="username">
            </div>
            <div class="form-group"> <!-- Input to enter the password-->
                <label for="pwd">Password :</label>
                <input type="password" class="form-control" name="password">
            </div>
            <button type="submit" class="btn btn-success" value="login">Login</button>
        </form>
    </div>
</body>

</html>